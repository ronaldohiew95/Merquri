# Merquri

1) Please download the PEM file to use it to ssh into the EC2 instance
2) EC2 keypair was created manually from ec2 console, so in order to connect to the ec2 instance from terraform, user has
   to either edit terraform file or create PEM key maually from console
3) Download the whole folder and run the following commands: 
terraform init
terraform plan
terraform apply
