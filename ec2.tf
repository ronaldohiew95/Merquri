#create EC2 instance
resource "aws_instance" "demo_ec2" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"
  associate_public_ip_address = true
  key_name = "demokeypair"
  #VPC
  subnet_id   = aws_subnet.demo-subnet-private-1.id
  # Security Group
    vpc_security_group_ids = ["${aws_security_group.ssh-allowed.id}"]

  tags = {
  Name = "Demo EC2"
}
}