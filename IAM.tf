resource "aws_iam_user" "user" {
  name = "test-user"
}

resource "aws_iam_policy" "policy" {
  name        = "test_policy"
  path        = "/"
  description = "My test policy"


  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({

    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr-public:BatchCheckLayerAvailability",
                "ecr-public:DescribeImageTags",
                "ecr-public:DescribeImages",
                "ecr-public:DescribeRegistries",
                "ecr-public:DescribeRepositories",
                "ecr-public:GetAuthorizationToken",
                "ecr-public:GetRegistryCatalogData",
                "ecr-public:GetRepositoryCatalogData",
                "ecr-public:GetRepositoryPolicy",
                "ecr-public:ListTagsForResource",
                "ecr:BatchCheck*",
                "ecr:BatchGet*",
                "ecr:Describe*",
                "ecr:Get*",
                "ecr:List*",
    		
		
            ],
            "Resource": "*"
        },
	{           
            "Effect": "Allow",
            "Action": [
                "ec2:Describe*",
                "ec2:Get*",
                "ec2:SearchTransitGatewayRoutes",
                "ec2messages:Get*",
            ],
            "Resource": [
                "*"
            ]
        },
	{
            "Effect": "Allow",
            "Action": [
                "ecs:Describe*",
                "ecs:List*",
            ],
            "Resource": [
                "*"
            ]
        },
	{
            "Effect": "Allow",
            "Action": [
                "iam:Generate*",
                "iam:Get*",
                "iam:List*",
                "iam:Simulate*",
            ],
            "Resource": [
                "*"
            ]
        },
	{
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:Describe*",
            ],
            "Resource": [
                "*"
            ]
        },
    ]
})
}

resource "aws_iam_user_policy_attachment" "test-attach" {
  user       = aws_iam_user.user.name
  policy_arn = aws_iam_policy.policy.arn
}
