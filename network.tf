resource "aws_internet_gateway" "prod-igw" {
    vpc_id = "${aws_vpc.demo.id}"

}

resource "aws_route_table" "prod-public-crt" {
    vpc_id = "${aws_vpc.demo.id}"
    
    route {
        //associated subnet can reach everywhere
        cidr_block = "0.0.0.0/0"
        //CRT uses this IGW to reach internet
        gateway_id = "${aws_internet_gateway.prod-igw.id}" 
    }


}
resource "aws_route_table_association" "demo-subnet-public-1"{
    subnet_id = "${aws_subnet.demo-subnet-public-1.id}"
    route_table_id = "${aws_route_table.prod-public-crt.id}"
}
resource "aws_route_table_association" "demo-subnet-private-1"{
    subnet_id = "${aws_subnet.demo-subnet-private-1.id}"
    route_table_id = "${aws_route_table.prod-public-crt.id}"
}
resource "aws_security_group" "ssh-allowed" {
    vpc_id = "${aws_vpc.demo.id}"
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    //If you do not add this rule, you can not reach the NGIX  
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0","118.189.0.0/16","116.206.0.0/16","203.25.0.0/16"]
    }


}

# Create a new load balancer
resource "aws_elb" "Demo-ELB" {
#to be created after EC2 initialising and creation of public subnet
depends_on = [
    aws_instance.demo_ec2,aws_subnet.demo-subnet-public-1,
  ]
  name               = "Demo-ELB"
  subnets = ["${aws_subnet.demo-subnet-public-1.id}"]
  security_groups = ["${aws_security_group.ssh-allowed.id}"]
listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }
listener {
    instance_port     = 8000
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }
  health_check {

    target              = "tcp:22"
    interval            = 60
    healthy_threshold   = 5
    unhealthy_threshold = 10
    timeout             = 50
}
  

  
  instances = [aws_instance.demo_ec2.id]

tags = {
    Name = "terraform-elb"
  }

}